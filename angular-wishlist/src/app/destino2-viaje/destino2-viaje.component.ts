import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-destino2-viaje',
  templateUrl: './destino2-viaje.component.html',
  styleUrls: ['./destino2-viaje.component.css']
})
export class Destino2ViajeComponent implements OnInit {

  @Input() nombre: string;

  constructor() { }

  ngOnInit(): void {
  }

}
