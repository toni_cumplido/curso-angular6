import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Destino2ViajeComponent } from './destino2-viaje.component';

describe('Destino2ViajeComponent', () => {
  let component: Destino2ViajeComponent;
  let fixture: ComponentFixture<Destino2ViajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Destino2ViajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Destino2ViajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
